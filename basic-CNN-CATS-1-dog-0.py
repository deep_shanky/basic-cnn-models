import cv2 #resizing img
import numpy as np
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
import tensorflow as tf
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
import os
import matplotlib.pyplot as plt
from random import shuffle
from tqdm import tqdm

train_dir = 'training_set/'
test_dir = 'test_set/'

model_name = 'catndogs-2conv'

def label_img(img):
	word_label = img.split('.')[-3]
	if word_label == 'cat': return [1,0]
	elif word_label == 'dog': return [0,1]

def create_train_data():
	train_data = []
	for img in tqdm(os.listdir(train_dir)):
		label = label_img(img)
		path = os.path.join(train_dir, img)
		img = cv2.resize(cv2.imread(path, cv2.IMREAD_GRAYSCALE), (50, 50))
		train_data.append([np.array(img), np.array(label)])

	shuffle(train_data)
	np.save('train_data.npy', train_data)
	return train_data

def process_test_data():
	test_data = []
	for img in tqdm(os.listdir(test_dir)):
		path = os.path.join(test_dir,img)
		img_num = img.split('.')[0]
		img = cv2.resize(cv2.imread(path, cv2.IMREAD_GRAYSCALE), (50, 50))
		test_data.append([np.array(img), img_num])

	np.save('test_data.npy', test_data)
	return test_data

train1 = create_train_data()

tf.reset_default_graph()

convnet = input_data(shape=[None, 50, 50, 1], name='input')

convnet = conv_2d(convnet, 32, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = conv_2d(convnet, 64, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = conv_2d(convnet, 32, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = conv_2d(convnet, 64, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = conv_2d(convnet, 32, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = conv_2d(convnet, 64, 2, activation='relu')
convnet = max_pool_2d(convnet, 2)

convnet = fully_connected(convnet, 1024, activation='relu')
convnet = dropout(convnet, 0.8)

convnet = fully_connected(convnet, 2, activation='softmax')
convnet = regression(convnet, optimizer='adam', learning_rate=0.001, loss='categorical_crossentropy', name='targets')

model = tflearn.DNN(convnet, tensorboard_dir='log')

if os.path.exists('{}.meta'.format(model_name)):
	model.load(model_name)
	print('model-loaded')

train = train1[:-500]
test = train1[-500:]

X = np.array([i[0] for i in train]).reshape(-1, 50, 50, 1)
Y = [i[1] for i in train]

test_x = np.array([i[0] for i in test]).reshape(-1, 50, 50, 1)
test_y = [i[1] for i in test]

model.fit({'input': X}, {'targets': Y}, n_epoch=5, validation_set=({'input': test_x}, {'targets': test_y}), 
    snapshot_step=500, show_metric=True, run_id=model_name)

model.save(model_name)

test1 = process_test_data()

fig = plt.figure()

for num, data in enumerate(test1[:12]):
	img_num = data[1]
	img_data = data[0]

	y = fig.add_subplot(3,4,num+1)
	orig = img_data
	data = img_data.reshape(50,50,1)

	model_out = model.predict([data])[0]

	if np.argmax(model_out) == 1:
		str_label = 'Dog'
	else:
		str_label = 'Cat'

	y.imshow(orig, cmap='gray')
	plt.title(str_label)
	y.axes.get_xaxis().set_visible(False)
	y.axes.get_yaxis().set_visible(False)

plt.show()